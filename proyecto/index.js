const express = require("express");
const bodyParser = require('body-parser');

const app = express();
app.listen(3000, () => {
 console.log("El servidor está inicializado en el puerto 3000");
});


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));



const events = [];

app.get('/events', function (req, res) {
const apiKey =  req.query['key'];

    if (apiKey !== '999111222') {
    res.status(403).send('sin permisos');
    return;
    }
    if (events.length === 0){
        res.status(400).send('no hay eventos');
    }
   
    const ciudad =  req.query['lugar'];
    const tipo = req.query['tipo'];
    if (ciudad !== undefined){
      const filteredciudad = events.filter(item => item['lugar'].toLowerCase() === ciudad.toLowerCase())
          res.send(filteredciudad);
    } else if (tipo !== undefined){
      const filteredTipo = events.filter(item => item['tipo'].toLowerCase() === tipo.toLowerCase())
          res.send(filteredTipo);
    }else{
      res.send(events)
    };
});

app.post('/events', (req, res) => {
    const apiKey =  req.query['key'];
    if (apiKey !== '999111222') {
        res.status(400).send('sin permisos');
        return;
        }

    const nombre = req.body['nombre'];
    const tipo = req.body['tipo'];
    const fecha = req.body['fecha'];
    const lugar = req.body['lugar'];
  
    let evento = {};
  
    evento['nombre'] = nombre;
    evento['tipo'] = tipo;
    evento['fecha'] = fecha;
    evento['lugar'] = lugar;

    
  
    for (let value of Object.values(evento)) {
      if (value === undefined || value.trim().length === 0) {
        res.status(400).send('faltan datos');
        return
      }
    }
    evento.data = {};

    for (let key of Object.keys(req.body)) {
      if (evento[key] === undefined) {
        const value = req.body[key].trim();
  
        if (value.length === 0) {
          res.status(400).send('adios');
          return;
        }
  
        evento.data[key] = value;
      }
    }

    events.push(evento);


    res.send(events); 
  
  })

 